**Reservo - Image Hosting Script Free**

Setup and manage your own image sharing service with Reservo - Image Hosting Script Free.

**About Reservo - Image Hosting Script Free**

Reservo - Image Hosting Script Free is the initial version of the premium image sharing script Reservo available at https://reservo.co

This is a community based release that is supplied without support or liability. You are free to use the image sharing script as you wish. The only restriction to this PHP script is that this README file must not be edited or deleted. 

**Features:**

* Multiple Uploader Tool now added.
* Upload progress indicator.
* Choose to use single or multi uploader as default via config.
* Option to protect images with a password once uploaded.
* Set watermark on every uploaded image, inc alpha and positioning.
* Unique delete link provided to the uploader to remove any images.
* New 'friendly' urls on image pages/image downloads.
* Written in php/mysql.
* All pages are created using Apaches mod-rewrite so any uploaded images all look like static pages within the search engines.
* Automatic generation of thumbnails along with 'copy & paste' code for the user.
* Automatic resize of images.
* Validation of duplicate content to stop same IP uploading same image multiple times.
* Basic admin area to approve/decline images. Images can also be approved as adult.
* Adult images display a warning message before user can view them.
* Total view for each image.
* Recent uploads page for public approved images.
* Date added, original filesize and description displayed on each image.
* Images which have not been viewed within 60 days (can be amended) are deleted.
* Hotlinking blocked by default.
* Max filesize, permit hotlinking and days to keep images can all be set via the config scripts.
* Basic design with original fireworks files included.
* Ability to display ads depending on whether an image is adult or not.

**Requirements**

* php 4.x
* mysql
* mod rewrite module on apache
* gd library with GIF write support
* HD/BW requirements will vary on your traffic, scripts require about 100kb.

**Installation**

Full instructions are given in _setup-instructions.txt file found within the repository.

**Contributions**

If you'd like to contribute to the project, please contact us via the project https://bitbucket.org/MFScripts/reservo-image-hosting-script-free

**License**

Reservo - Image Hosting Script Free is copyrighted by http://mfscripts.com and is released under the MIT License http://opensource.org/licenses/MIT. You are free to use the image sharing script as you wish. The only restriction to this PHP script is that this README file must not be edited or deleted. 

**Support**

This code is released without any support and as-is. Any support requests on this version will be removed. For community driven support please use the project https://bitbucket.org/MFScripts/reservo-image-hosting-script-free